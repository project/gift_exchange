README.txt 
==========

A module that implements a Gift Exchange for your Christmas
holiday needs. It uses drupal nodes, entity reference, views and ajax.

It implements mail sending, exceptions and multiple gifts per person.

@todo: Add mail functionality on status change.

AUTHOR/MAINTAINER 
====================== 
Author: Thanos Nokas(Matrixlord) 
Maintainer: Thanos Nokas(Matrixlord) (https://drupal.org/user/1538394)