<?php

/**
 * @file
 * Admin settings file for Gift Exchange module.
 */

/**
 * Admin setings form.
 */
function gift_exchange_settings_form($form, &$form_state) {

  $select_items = array(
    1 => 1,
    5 => 5,
    10 => 10,
    20 => 20,
    50 => 50,
  );

  $select_items_days = array(
    0 => 0,
    30 => t('1 month'),
    90 => t('3 months'),
    180 => t('6 months'),
    365 => t('1 year'),
  );

  $form = array();

  // Parties to process per cron run.
  $form['ge_parties_per_cron'] = array(
    '#type' => 'select',
    '#title' => t('Parties to process per cron run'),
    '#options' => $select_items,
    '#default_value' => variable_get('ge_parties_per_cron', 5),
    '#description' => t('How many parties to process per cron run.'),
    '#element_validate' => array('element_validate_integer_positive'),
  );

  // Members to process per cron run.
  $form['ge_members_per_cron'] = array(
    '#type' => 'select',
    '#title' => t('Members of party to process per cron run'),
    '#options' => $select_items,
    '#default_value' => variable_get('ge_members_per_cron', 5),
    '#description' => t('How many people the generator will try to assign Gift Exchange per party per cron run.'),
    '#element_validate' => array('element_validate_integer_positive'),
  );

  // Mails to send per cron run.
  $form['ge_mails_per_cron'] = array(
    '#type' => 'select',
    '#title' => t('Mails to send per cron run'),
    '#options' => $select_items,
    '#default_value' => variable_get('ge_mails_per_cron', 5),
    '#description' => t('How many mails will be sent per cron run.'),
    '#element_validate' => array('element_validate_integer_positive'),
  );

  // Circles.
  $form['ge_circles'] = array(
    '#type' => 'select',
    '#title' => t('Circles'),
    '#options' => $select_items,
    '#default_value' => variable_get('ge_circles', 5),
    '#description' => t('How many circles a party will run before it is declared unsolvable.'),
    '#element_validate' => array('element_validate_integer_positive'),
  );

  // Auto achive parties.
  $form['ge_archive'] = array(
    '#type' => 'select',
    '#title' => t('Auto archive'),
    '#options' => $select_items_days,
    '#default_value' => variable_get('ge_archive', 365),
    '#description' => t('How many days until a party is archived. Choose 0 to disable.'),
    '#element_validate' => array('element_validate_integer_positive'),
  );

  return system_settings_form($form);
}
