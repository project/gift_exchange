<?php

/**
 * @file
 * Functions file for Gift Exchange module.
 */

/**
 * Breadcrumb for Gift Exchange.
 *
 * @param $breadcrumb_extend
 */
function gift_exchange_breadcrumb($breadcrumb_extend = array()) {
  // Build Breadcrumbs.
  $breadcrumb = array();
  $breadcrumb[] = l(t('Home'), '<front>');

  $breadcrumb = array_merge($breadcrumb, $breadcrumb_extend);

  // Set Breadcrumbs.
  drupal_set_breadcrumb($breadcrumb);
}

/**
 * Create a new party.
 *
 * @param $params
 *   Array containing party parameters.
 * array(
 *  'uid',
 *  'title',
 *  'body',
 *  'field' => array(
 *    'field_ge_status',
 *    'field_ge_gifts',
 *    'field_ge_send_mails',
 *  )
 * )
 *
 * @return int or false
 *   Member nid or FALSE.
 */
function gift_exchange_create_party($params) {

  // Create an Entity.
  $e = entity_create('node', array('type' => 'gift_exchange'));
  $e->uid = $params['uid'];
  $node_wrapper = entity_metadata_wrapper('node', $e);
  $node_wrapper->title = $params['title'];
  $node_wrapper->body = array('value' => $params['body']);

  // Set fields.
  foreach($params['fields'] as $key => $value) {
    $node_wrapper->{$key}->set($value);
  }

  // Save the node.
  $node_wrapper->save();
  return $node_wrapper->getIdentifier();
}

/**
 * Get Gift Exchange Party status.
 *
 * @param $party_nid
 *   Gift Exchange Party node id.
 *
 * @return string
 *   Status.
 */
function gift_exchange_get_party_status($party_nid) {
  $party = node_load($party_nid);

  $status = isset($party->field_ge_status[LANGUAGE_NONE][0]['value']) ?
    $party->field_ge_status[LANGUAGE_NONE][0]['value'] : 'setup';

  return $status;
}

/**
 * Get Gift Exchange Party status for display.
 *
 * @param $party_nid
 *   Gift Exchange Party node id.
 *
 * @return string
 *   State.
 */
function gift_exchange_get_party_status_display($party_nid) {
  $status = gift_exchange_get_party_status($party_nid);

  switch ($status) {
    case 'setup':
      $status = t('Setup');
      break;

    case 'processing':
      $status = t('Processing');
      break;

    case 'sending_mail':
      $status = t('Sending mails');
      break;

    case 'processed':
      $status = t('Processed');
      break;

    case 'reset':
      $status = t('Resetting');
      break;

    case 'unsolvable':
      $status = t('Unsolvable');
      break;

    case 'unsolvable_reset':
      $status = t('Resetting');
      break;

    case 'archived':
      $status = t('Archived');
      break;

  }

  return $status;
}

/**
 * Set Gift Exchange Party status.
 *
 * @param $party_nid
 *   Gift Exchange Party node id.
 * @param $state
 *   New state.
 */
function gift_exchange_set_party_status($party_nid, $new_status = 'setup') {
  $party = node_load($party_nid);
  $previous_status = $party->field_ge_status[LANGUAGE_NONE][0]['value'];
  $party->field_ge_status[LANGUAGE_NONE][0]['value'] = $new_status;

  // Creating hook to alter contents of the node.
  $party_altered = module_invoke_all('ge_party_status_changed', $party, $previous_status, $new_status);
  $party = $party_altered ? $party_altered : $party;

  node_save($party);
}

/**
 * Get party members count.
 *
 * @param $party_nid
 *   Gift Exchange Party node id.
 */
function gift_exchange_get_party_members_count($party_nid) {
  $query = db_select('node', 'n')
    ->fields('n', array('nid'))
    ->condition('gem.field_gift_exchange_target_id', $party_nid)
    ->condition('n.type', 'gift_exchange_member');

  // Party reference
  $query->join('field_data_field_gift_exchange', 'gem',
    'gem.entity_id = n.nid');

  $result = $query->execute()->rowCount();

  return $result;
}

/**
 * Get party connections count.
 *
 * @param $party_nid
 *   Gift Exchange Party node id.
 * @param $sent_mail
 *   If there has been sent mail.
 */
function gift_exchange_get_party_connections_count($party_nid, $sent_mail = FALSE) {
  $query = db_select('node', 'n')
    ->fields('n', array('nid'))
    ->condition('gem.field_gift_exchange_target_id', $party_nid)
    ->condition('n.type', 'gift_exchange_connection');

  // Party reference.
  $query->join('field_data_field_gift_exchange', 'gem',
    'gem.entity_id = n.nid');
  
  // If there has been sent mail.
  if ($sent_mail) {
    $query->join('field_data_field_gec_mail_sent', 'ms',
      'ms.entity_id = n.nid');
    $query->condition('ms.field_gec_mail_sent_value', 1);
  }

  $result = $query->execute()->rowCount();

  return $result;
}

/**
 * Get party exceptions count.
 *
 * @param $party_nid
 *   Gift Exchange Party node id.
 */
function gift_exchange_get_party_exceptions_count($party_nid) {
  $query = db_select('node', 'n')
    ->fields('n', array('nid'))
    ->condition('gem.field_gift_exchange_target_id', $party_nid)
    ->condition('n.type', 'gift_exchange_exception');

  // Party reference
  $query->join('field_data_field_gift_exchange', 'gem',
    'gem.entity_id = n.nid');

  $result = $query->execute()->rowCount();

  return $result;
}

/**
 * Create a new Member.
 *
 * @param $name
 *   Name of the Member.
 * @param $email
 *   Email of the Member.
 * @param $party
 *   Gift Exchange Party that Member belongs.
 *
 * @return int or false
 *   Member nid or FALSE.
 */
function gift_exchange_create_member($name, $email = '', $party) {

  // Check if member with this mail exists in this party.
  if (!gift_exchange_get_member_name_email_is_in_party($name, $email, $party->nid)) {
    // Create new member.

    $node = new stdClass();
    $node->title = $name;
    $node->type = 'gift_exchange_member';
    node_object_prepare($node);
    $node->language = LANGUAGE_NONE;
    $node->uid = $party->uid;
    $node->status = 1;
    $node->promote = 0;
    $node->comment = 0;

    $node->field_gift_exchange[$node->language][] = array(
      'target_id' => $party->nid,
      'target_type' => 'node',
    );

    if ($email) {
      $node->field_gem_email[$node->language][] = array(
        'value' => $email,
      );
    }

    // Creating hook to alter contents of the node.
    $node_altered = module_invoke_all('ge_member_new', $node);
    $node = $node_altered ? $node_altered : $node;

    // Prepare node for saving.
    $node = node_submit($node);
    node_save($node);

    return $node->nid;
  }
  else {
    return FALSE;
  }
}

/**
 * Update Member.
 *
 * @param $member_nid
 *   Member nid.
 * @param $name
 *   Name of the Member.
 * @param $email
 *   Email of the Member.
 *
 * @return int or false
 *   Member nid or FALSE.
 */
function gift_exchange_update_member($member_nid, $name, $email = '') {
  $node = node_load($member_nid);
  $node_wrapper = entity_metadata_wrapper('node', $node);

  $node_wrapper->title->set($name);
  $node_wrapper->field_gem_email->set($email);

  $node_wrapper->save();
}

/**
 * Get if member is in party.
 *
 * @param $member_nid
 *   Member node id.
 * @param $party_nid
 *   Exchange Party nid.
 *
 * @return boolean
 *   Member is in party or not.
 */
function gift_exchange_get_member_is_in_party($member_nid, $party_nid)  {

  $query = db_select('node', 'm')
    ->fields('m', array('nid'))
    ->condition('m.nid', $member_nid)
    ->condition('gem.field_gift_exchange_target_id', $party_nid);

  // Member of the party.
  $query->join('field_data_field_gift_exchange', 'gem',
    'gem.entity_id = m.nid');

  $result = $query->execute()->rowCount();

  return $result > 0;
}

/**
 * Get if there is a member with this mail in party.
 *
 * @param $email
 *   Member email.
 * @param $party_nid
 *   Exchange Party nid.
 *
 * @return boolean
 *   Member is in party or not.
 */
function gift_exchange_get_member_email_is_in_party($email, $party_nid)  {

  // Check if member with this mail exists in this party.
  $query = db_select('node', 'm')
    ->fields('m', array('nid'))
    ->condition('gem.field_gift_exchange_target_id', $party_nid)
    ->condition('geme.field_gem_email_value', $email)
    ->condition('m.type', 'gift_exchange_member');

  // Member of the party.
  $query->join('field_data_field_gift_exchange', 'gem',
    'gem.entity_id = m.nid');
  // This mail.
  $query->join('field_data_field_gem_email', 'geme',
    'geme.entity_id = m.nid');

  $result = $query->execute()->rowCount();

  return $result > 0;
}

/**
 * Get if there is a member with this name and mail in party.
 *
 * @param $name
 *   Member name.
 * @param $email
 *   Member email.
 * @param $party_nid
 *   Exchange Party nid.
 * @param $member_nid
 *   Current member nid to rule out.
 *
 * @return boolean
 *   Member is in party or not.
 */
function gift_exchange_get_member_name_email_is_in_party($name, $email = '', $party_nid, $member_nid = NULL)  {

  // Check if member with this mail exists in this party.
  $query = db_select('node', 'm')
    ->fields('m', array('nid'))
    ->condition('gem.field_gift_exchange_target_id', $party_nid)
    ->condition('m.type', 'gift_exchange_member');
    
  // Remove current member from search.
  if ($member_nid) {
    $query->condition('m.nid', $member_nid, '!=');
  }

  // Member of the party.
  $query->join('field_data_field_gift_exchange', 'gem',
    'gem.entity_id = m.nid');

  // If mail is passed.
  if ($email != '') {
    $or = db_or();
    $query->leftjoin('field_data_field_gem_email', 'geme',
      'm.nid = geme.entity_id');
    
    $or->condition('geme.field_gem_email_value', $email);
    $or->condition('m.title', $name);
    $query->condition($or);
  }
  else {
   $query->condition('m.title', $name);
  }
  
  $result = $query->execute();
  
  $result = $result->rowCount();
  
  return $result > 0;
}

/**
 * Get member by email.
 *
 * @param $email
 *   Member email.
 * @param $party_nid
 *   Exchange Party nid.
 *
 * @return int
 *   Member nid.
 */
function gift_exchange_get_member_by_email($email, $party_nid)  {

  $query = db_select('node', 'm')
    ->fields('m', array('nid'))
    ->condition('ge.field_gift_exchange_target_id', $party_nid)
    ->condition('gem.field_gem_email_value', $email)
    ->condition('m.type', 'gift_exchange_member');

  // Mail.
  $query->join('field_data_field_gem_email', 'gem',
    'gem.entity_id = m.nid');

  // Member of the party.
  $query->join('field_data_field_gift_exchange', 'ge',
    'ge.entity_id = m.nid');

  $member = $query->execute()->fetchAssoc();
  
  if (isset($member['nid'])) {
    return $member['nid'];
  }
  
  return NULL;
}

/**
 * Get member email(If exists).
 *
 * @param $member_nid
 *   Member email.
 *
 * @return int
 *   Member nid.
 */
function gift_exchange_get_member_email($member_nid) {

  $query = db_select('node', 'm')
    ->fields('gem', array('field_gem_email_value'))
    ->condition('m.nid', $member_nid)
    ->condition('m.type', 'gift_exchange_member');

  // Mail.
  $query->join('field_data_field_gem_email', 'gem',
    'gem.entity_id = m.nid');

  $member = $query->execute()->fetchAssoc();
  
  if (isset($member['field_gem_email_value'])) {
    return $member['field_gem_email_value'];
  }
  
  return NULL;
}

/**
 * Get if member with this mail share a party with member nid.
 *
 * @param $email
 *   Member email.
 * @param $member_nid
 *   Member nid.
 *
 * @return boolean
 *   Member is in the same party or not.
 */
function gift_exchange_get_member_email_share_party($email, $member_nid)  {

  // Check if member with this mail exists in this party.
  $query = db_select('node', 'm')
    ->fields('m', array('nid'))
    ->condition('m.type', 'gift_exchange_member')
    ->condition('gem2.field_gift_exchange_target_id', 'gem.field_gift_exchange_target_id')
    ->condition('geme.field_gem_email_value', $email);

  // Member of the party.
  $query->join('field_data_field_gift_exchange', 'gem',
    'gem.entity_id = m.nid');

  // This mail.
  $query->join('field_data_field_gem_email', 'geme',
    'geme.entity_id = m.nid');

  // Get other member.
  $query->join('node', 'm2','m2.nid = ' . $member_nid);

  // Get same party.
  $query->join('field_data_field_gift_exchange', 'gem2',
    'gem2.entity_id = m2.nid');

  $result = $query->execute()->rowCount();

  return $result > 0;
}

/**
 * Get array with receivers Member nids for a user by email.
 *
 * @param $email
 *   Member email.
 * @param $party_nid
 *   Party nid.
 *
 * @return array
 *   Member nids array.
 */
function gift_exchange_get_member_receivers($email, $party_nid) {
  // Check if member with this mail exists in this party.
  $query = db_select('node', 'm')
    ->fields('ct', array('field_gec_to_target_id'))
    ->condition('gem.field_gift_exchange_target_id', $party_nid)
    ->condition('m.type', 'gift_exchange_member')
    ->condition('geme.field_gem_email_value', $email);

  // Member of the party.
  $query->join('field_data_field_gift_exchange', 'gem',
    'gem.entity_id = m.nid');

  // This mail.
  $query->join('field_data_field_gem_email', 'geme',
    'geme.entity_id = m.nid');

  // From.
  $query->join('field_data_field_gec_from', 'cf',
    'cf.field_gec_from_target_id = m.nid');

  // To.
  $query->join('field_data_field_gec_to', 'ct',
    'cf.entity_id = ct.entity_id');

  $receivers = $query->execute()->fetchAll();

  $receivers_array = array();
  foreach ($receivers as $receiver) {
    $receivers_array[] = $receiver->field_gec_to_target_id;
  }

  return $receivers_array;
}

/**
 * Get member nid receivers.
 *
 * @param $member_nid
 *   Member nid.
 * @param $entities
 *   Load full entities.
 *
 * @return array
 *   As nids or entities.
 */
function gift_exchange_get_member_nid_receivers($member_nid, $entities = FALSE) {

  // Check if member with this mail exists in this party.
  $query = db_select('node', 'm')
    ->fields('ct', array('field_gec_to_target_id'))
    ->condition('m.nid', $member_nid)
    ->condition('m.type', 'gift_exchange_member');

  // Member of the party.
  $query->join('field_data_field_gift_exchange', 'gem',
    'gem.entity_id = m.nid');

  // From.
  $query->join('field_data_field_gec_from', 'cf',
    'cf.field_gec_from_target_id = m.nid');

  // To.
  $query->join('field_data_field_gec_to', 'ct',
    'cf.entity_id = ct.entity_id');

  $receivers = $query->execute()->fetchAll();

  $receivers_array = array();
  foreach ($receivers as $receiver) {
    $receivers_array[] = $entities ? node_load($receiver->field_gec_to_target_id) : $receiver->field_gec_to_target_id;
  }

  return $receivers_array;
}

/**
 * Get array with givers Member nids for a user by email, for a party or all.
 *
 * @param $email
 *   Member email.
 * @param $party_nid
 *   Party nid.
 * @param $active
 *   Only for active parties.
 *
 * @return array
 *   Member nids array.
 */
function gift_exchange_get_givers($email, $party_nid = NULL, $active = TRUE) {
  // Check if member with this mail exists in this party.
  $query = db_select('node', 'm')
    ->fields('m', array('title')) // Receiver title(name).
    ->fields('m', array('nid')) // Receiver nid.
    ->fields('cf', array('field_gec_from_target_id'))
    ->fields('ge', array('field_gift_exchange_target_id'))
    ->condition('m.type', 'gift_exchange_member')
    ->condition('geme.field_gem_email_value', $email);

  // Get for specific party.
  if ($party_nid) {
    $query->condition('ge.field_gift_exchange_target_id', $party_nid);
  }

  // Only active parties.
  if ($active) {
    $or = db_or();
    $or->condition('ges.field_ge_status_value', 'processed');
    $or->condition('ges.field_ge_status_value', 'sending_mail');
    $query->condition($or);
  }

  // This mail.
  $query->join('field_data_field_gem_email', 'geme',
    'geme.entity_id = m.nid');

  // To.
  $query->join('field_data_field_gec_to', 'ct',
    'ct.field_gec_to_target_id = m.nid');

  // From.
  $query->join('field_data_field_gec_from', 'cf',
    'cf.entity_id = ct.entity_id');

  // Party.
  $query->join('field_data_field_gift_exchange', 'ge',
    'ge.entity_id = ct.entity_id');

  // Party status.
  $query->join('field_data_field_ge_status', 'ges',
    'ges.entity_id = ge.field_gift_exchange_target_id');

  $givers = $query->execute()->fetchAll();

  $givers_array = array();
  foreach ($givers as $giver) {
    $givers_array[] = array(
      'receiver_name' => $giver->title,
      'receiver_nid' => $giver->nid,
      'giver_nid' => $giver->field_gec_from_target_id,
      'party_nid' => $giver->field_gift_exchange_target_id,
    );
  }

  return $givers_array;
}

/**
 * Get if member is user by email.
 *
 * @param $email
 *   Member email.
 *
 * @return int
 *   User id or NULL.
 */
function gift_exchange_get_user_by_email($email) {

  $query = db_select('users', 'u')
    ->fields('u', array('uid'))
    ->condition('u.mail', $email);
  $result = $query->execute()->fetchAssoc();

  if (isset($result['uid'])) {
    return $result['uid'];
  }

  return NULL;
}

/**
 * Create a new Connection.
 *
 * @param $giver_nid
 *   Giver nid.
 * @param $receiver_nid
 *   Receiver nid.
 * @param $party_nid
 *   Gift Exchange Party that Members belong.
 */
function gift_exchange_create_connection($giver_nid, $receiver_nid, $party_nid) {

  // Create new connection.
  $party = node_load($party_nid);

  $node = new stdClass();
  $node->title = 'Connection ' . $party_nid . '-' . $giver_nid . '-' . $receiver_nid;
  $node->type = 'gift_exchange_connection';
  node_object_prepare($node);
  $node->language = LANGUAGE_NONE;
  $node->uid = $party->uid;
  $node->status = 1;
  $node->promote = 0;
  $node->comment = 0;

  $node->field_gift_exchange[$node->language][] = array(
    'target_id' => $party_nid,
    'target_type' => 'node',
  );

  $node->field_gec_from[$node->language][] = array(
    'target_id' => $giver_nid,
    'target_type' => 'node',
  );

  $node->field_gec_to[$node->language][] = array(
    'target_id' => $receiver_nid,
    'target_type' => 'node',
  );

  // Creating hook to alter contents of the node.
  $node_altered = module_invoke_all('ge_connection_new', $node);
  $node = $node_altered ? $node_altered : $node;

  // Prepare node for saving.
  $node = node_submit($node);
  node_save($node);
}

/**
 * Get connection.
 *
 * @param $giver_nid
 *   Member node id.
 * @param $receiver_nid
 *   Member Party nid.
 *
 * @return mixed
 *   Nid of connection or FALSE.
 */
function gift_exchange_get_connection($giver_nid, $receiver_nid)  {

  $query = db_select('node', 'c')
    ->fields('c', array('nid'))
    ->condition('cf.field_gec_from_target_id', $giver_nid)
    ->condition('ct.field_gec_to_target_id', $receiver_nid)
    ->condition('c.type', 'gift_exchange_connection');

  // From.
  $query->join('field_data_field_gec_from', 'cf',
    'cf.entity_id = c.nid');

  // To.
  $query->join('field_data_field_gec_to', 'ct',
    'ct.entity_id = c.nid');

  $result = $query->execute()->fetchAssoc();

  return isset($result['nid']) ? node_load($result['nid']) : FALSE;
}

/**
 * Get exception.
 *
 * @param $giver_nid
 *   Member node id.
 * @param $receiver_nid
 *   Member Party nid.
 *
 * @return mixed
 *   Nid of exception or FALSE.
 */
function gift_exchange_get_exception($giver_nid, $receiver_nid)  {

  $query = db_select('node', 'e')
    ->fields('e', array('nid'))
    ->condition('ef.field_gee_from_target_id', $giver_nid)
    ->condition('et.field_gee_to_target_id', $receiver_nid)
    ->condition('e.type', 'gift_exchange_exception');

  // From.
  $query->join('field_data_field_gee_from', 'ef',
    'ef.entity_id = e.nid');

  // To.
  $query->join('field_data_field_gee_to', 'et',
    'et.entity_id = e.nid');

  $result = $query->execute()->fetchAssoc();

  return isset($result['nid']) ? node_load($result['nid']) : FALSE;
}

/**
 * Create a new Exception.
 *
 * @param $giver_nid
 *   Giver nid.
 * @param $receiver_nid
 *   Receiver nid.
 * @param $party_nid
 *   Gift Exchange Party that Members belong.
 */
function gift_exchange_create_exception($giver_nid, $receiver_nid, $party_nid) {

  // Create new exception.
  $party = node_load($party_nid);

  $node = new stdClass();
  $node->title = 'Exception ' . $party_nid . '-' . $giver_nid . '-' . $receiver_nid;
  $node->type = 'gift_exchange_exception';
  node_object_prepare($node);
  $node->language = LANGUAGE_NONE;
  $node->uid = $party->uid;
  $node->status = 1;
  $node->promote = 0;
  $node->comment = 0;

  $node->field_gift_exchange[$node->language][] = array(
    'target_id' => $party_nid,
    'target_type' => 'node',
  );

  $node->field_gee_from[$node->language][] = array(
    'target_id' => $giver_nid,
    'target_type' => 'node',
  );

  $node->field_gee_to[$node->language][] = array(
    'target_id' => $receiver_nid,
    'target_type' => 'node',
  );

  // Creating hook to alter contents of the node.
  $node_altered = module_invoke_all('ge_exception_new', $node);
  $node = $node_altered ? $node_altered : $node;

  // Prepare node for saving.
  $node = node_submit($node);
  node_save($node);
}

/**
 * Get parties by status.
 *
 * @param $status
 *   Party status.
 * @param $limit
 *   Override limit.
 *
 * @return array
 *   Array of Gift Exchange Parties.
 */
function gift_exchange_get_parties($status = 'processing', $limit = NULL) {
  $parties = array();

  if ($limit == NULL) {
    $limit = variable_get('ge_parties_per_cron', 5);
  }

  // Get Parties that are in processing.
  $query = new EntityFieldQuery();
  $query->entityCondition('entity_type', 'node')
    ->entityCondition('bundle', 'gift_exchange')
    ->propertyCondition('status', 1)
    ->fieldCondition('field_ge_status', 'value', $status)
    ->propertyOrderBy('created', 'ASC')
    ->range(0, $limit);
  $result = $query->execute();

  if (isset($result['node'])) {
    foreach ($result['node'] as $party) {
      $parties[] = node_load($party->nid);
    }
  }

  return $parties;
}

/**
 * Add mails to send to queue.
 */
function gift_exchange_mails_add_to_queue() {

  // Get parties.
  $parties = gift_exchange_get_parties('sending_mail');

  // Process parties.
  foreach ($parties as $party) {
    _gift_exchange_mails_party($party);
  }
}

/**
 * Add mails per party to queue.
 *
 * @param $party
 *   Node of party to process.
 */
function _gift_exchange_mails_party($party) {
  $limit = variable_get('ge_mails_per_cron', 5);
  $party_nid = $party->nid;

  // Get members.
  $query = db_select('node', 'm')
    ->fields('m', array('nid'))
    ->condition('gem.field_gift_exchange_target_id', $party_nid)
    ->condition('m.type', 'gift_exchange_member')
    ->range(0, $limit);

  // Member of the party.
  $query->join('field_data_field_gift_exchange', 'gem',
    'gem.entity_id = m.nid');

  // Has unsent connections.
  $query->join('field_data_field_gec_from', 'cf',
    'cf.field_gec_from_target_id = m.nid');
  $query->leftjoin('field_data_field_gec_mail_sent', 'ms',
    'cf.entity_id = ms.entity_id');

  // Get receiver.
  $query->join('field_data_field_gec_to', 'ct',
    'ct.entity_id = cf.entity_id');
  
  // Giver has mail.
  $query->join('field_data_field_gem_email', 'mfm',
    'mfm.entity_id = m.nid');
  $query->isNotNull('mfm.field_gem_email_value');

  // Sent mail false or null.
  $or = db_or();
  $or->condition('ms.field_gec_mail_sent_value', FALSE)
    ->isNull('ms.field_gec_mail_sent_value');
  $query->condition($or);

  $query->addExpression('GROUP_CONCAT(ct.field_gec_to_target_id SEPARATOR \',\')', 'receivers');

  $query->groupBy('m.nid');

  $givers = $query->execute()->fetchAll();

  // If no more givers. Change party status.
  if (count($givers) == 0) {
    gift_exchange_set_party_status($party_nid, 'processed');
  }

  // Send mails and set connections as sent.
  foreach ($givers as $giver_nid) {
    $giver = node_load($giver_nid->nid);
    $receivers = array();
    $receivers_temp = explode(',', $giver_nid->receivers);
    foreach ($receivers_temp as $receiver_nid) {
      $receiver = node_load($receiver_nid);
      $receivers[] = $receiver;

      $connection = gift_exchange_get_connection($giver->nid, $receiver->nid);
      $connection->field_gec_mail_sent[LANGUAGE_NONE][0]['value'] = TRUE;
      node_save($connection);
    }

    // Send mail for connection. Add to queue to send later.
    gift_exchange_mail_member_add_to_queue($giver, $receivers);
  }
}

/**
 * Send mail for connection. Add to queue to send later.
 *
 * @param $giver
 *   The giver of the gift, but receiver of mail.
 * @param $receivers
 *   The receivers of the gift.
 */
function gift_exchange_mail_member_add_to_queue($giver, $receivers) {
  $item = array(
		  'giver' => $giver,
		  'receivers' => $receivers,
  );

	// Put everything in a queue for processing.
	$queue = DrupalQueue::get('gift_exchange_mail_member_queue');

	// There is no harm in trying to recreate existing.
	$queue->createQueue();

	// Add item to queue.
	$queue->createItem($item);
}

/**
 * Processing mails to members from queue.
 */
function gift_exchange_mails_process_queue() {
  $queue = DrupalQueue::get('gift_exchange_mail_member_queue');

	// There is no harm in trying to recreate existing.
	$queue->createQueue();

	$limit = variable_get('ge_mails_per_cron', 5);

	// Indicator to stay on limit.
	$i = 0;

	while ($queue->numberOfItems() > 0 && $i < $limit) {

		// Get item.
		$item = $queue->claimItem();
		// Check for something that should exist to proceed.
		if (isset($item->data['giver'])) {
		  $giver = $item->data['giver'];
		  $receivers = $item->data['receivers'];

    // Remove item from queue.
    $queue->deleteItem($item);

		  $party = node_load($giver->field_gift_exchange[LANGUAGE_NONE][0]['target_id']);

		  // Log message.
		  watchdog('gift_exchange', 'Sending mail to member: :user', array(':user' => $giver->title));

  		// Creating an array and then converting it to string.
  		$receivers_string = array();
  		foreach ($receivers as $receiver) {
        $receivers_string[] = $receiver->title;
  		}
  		$receivers_string = implode(', ', $receivers_string);

  		// Send mail
  		$body = array();
  		$text = t('You will give a present to: ') . $receivers_string;
  		$body[] = $text;
  		$to = $giver->field_gem_email[LANGUAGE_NONE][0]['value'];
  		$subject = $party->title;
  		$module = 'gift_exchange';
  		$key = 'gift_exchange_member_mail';
  		$params = array(
        'body' => $body,
        'subject' => $subject,
        'giver' => $giver,
        'receivers' => $receivers,
        'party' => $party,
      );
  		$mail_message = drupal_mail($module, $key, $to, language_default(), $params);
		}

   // If item still exists, remove it.
   if ($item) {
     // Remove item from queue.
     $queue->deleteItem($item);
   }

		// Add 1 to indicator.
		$i++;
	}
}

/**
 * Processing mails to author from queue.
 */
function gift_exchange_mails_author_process_queue() {
  $queue = DrupalQueue::get('gift_exchange_mail_author_queue');

	// There is no harm in trying to recreate existing.
	$queue->createQueue();

	$limit = variable_get('ge_mails_per_cron', 5);

	// Indicator to stay on limit.
	$i = 0;

	while ($queue->numberOfItems() > 0 && $i < $limit) {

		// Get item.
		$item = $queue->claimItem();
		// Check for something that should exist to proceed.
		if (isset($item->data['party'])) {
		  $party = $item->data['party'];

    // Remove item from queue.
    $queue->deleteItem($item);

		  $status = gift_exchange_get_party_status_display($party->nid);
		  $user = user_load($party->uid);

		  watchdog('gift_exchange', 'Sending mail to user: :user', array(':user' => $user->uid . ' - ' . $user->mail));

  		// Send mail
  		$body = array();
  		$body[] =  t('Your party :party changed status to :status.',
  		  array(':party' => $party->title, ':status' => $status));

  		// Provide auto login url if module exists.
  		if (module_exists('auto_login_url')) {
  		  $destination = 'gift-exchange/my-parties/' . $party->nid;
  		  $url = auto_login_url_create($user->uid, $destination, TRUE);
  		  $body[] = l(t('Check it here'), $url, array('absolute' => TRUE));
  		}

  		$to = $user->mail;
  		$subject = $party->title . ' -> ' . $status;
  		$module = 'gift_exchange';
  		$key = 'gift_exchange_author_mail';
  		$params = array(
        'body' => $body,
        'subject' => $subject,
      );
  		$mail_message = drupal_mail($module, $key, $to, language_default(), $params);
		}

   // If item still exists, remove it.
   if ($item) {
     // Remove item from queue.
     $queue->deleteItem($item);
   }

		// Add 1 to indicator.
		$i++;
	}
}

/**
 * Process Gift Exchange Connections.
 */
function gift_exchange_set_connections() {
  // Get parties to process that are in "processing" state.
  $parties = gift_exchange_get_parties('processing');

  // Process parties.
  foreach ($parties as $party) {
    _gift_exchange_set_connections_party($party);
  }
}

/**
 * Process party.
 *
 * @param $party
 *   Node of party to process.
 */
function _gift_exchange_set_connections_party($party) {

  $limit = variable_get('ge_members_per_cron', 5);
  $party = node_load($party->nid);
  $party_nid = $party->nid;
  $gifts = $party->field_ge_gifts[LANGUAGE_NONE][0]['value'];
  // @todo: Adjust maximum gifts to (members - 1) if bigger.

  // Get members that
  //   belong to this party
  //   haven't filled all connection on this party
  //   order by with most exceptions DESC,
  //
  // Get members of party that are to give gifts.
  $query = db_select('node', 'm')
    ->fields('m', array('nid'))
    ->condition('gem.field_gift_exchange_target_id', $party_nid)
    ->condition('m.type', 'gift_exchange_member')
    ->range(0, $limit);

  // Member of the party.
  $query->join('field_data_field_gift_exchange', 'gem',
    'gem.entity_id = m.nid');

  // Count exceptions.
  $exceptions = '(SELECT COUNT(*) FROM {field_data_field_gee_from} et WHERE et.field_gee_from_target_id = m.nid)';
  $query->addExpression($exceptions, 'exceptions');

  // Count connections
  $connections = '(SELECT COUNT(*) FROM {field_data_field_gec_from} ct WHERE ct.field_gec_from_target_id = m.nid)';
  $query->addExpression($connections, 'connections');
  $query->havingCondition('connections', $gifts, '<');

  // Order by.
  $query->orderBy('exceptions', 'DESC')
    ->orderBy('connections', 'ASC')
    ->orderRandom();

  // Group by.
  $query->groupBy('m.nid');

  $givers = $query->execute()->fetchAll();

  // If no more givers. Change party status.
  if (count($givers) == 0) {
    if ($party->field_ge_send_mails[LANGUAGE_NONE][0]['value']) {
      gift_exchange_set_party_status($party_nid, 'sending_mail');
    }
    else {
      gift_exchange_set_party_status($party_nid, 'processed');
    }
  }

  // Create connections.
  foreach ($givers as $giver) {
    // Get receivers with most exceptions on them, that haven't received the
    // maximum amount of gifts.
    $query = db_select('node', 'm')
      ->fields('m', array('nid'))
      ->condition('gem.field_gift_exchange_target_id', $party_nid)
      ->condition('m.type', 'gift_exchange_member')
      ->condition('m.nid', $giver->nid, '!=')
      ->range(0, 1);

    // Member of the party.
    $query->join('field_data_field_gift_exchange', 'gem',
      'gem.entity_id = m.nid');

    // Count exceptions.
    $exceptions = '(SELECT COUNT(*) FROM {field_data_field_gee_to} et WHERE et.field_gee_to_target_id = m.nid)';
    $query->addExpression($exceptions, 'exceptions');

    // Remove exceptions.
    $exceptions = db_select('field_data_field_gee_to', 'et')
      ->fields('et', array('field_gee_to_target_id'));
    $exceptions->join('field_data_field_gee_from', 'ef', 'ef.entity_id = et.entity_id');
    $exceptions->condition('ef.field_gee_from_target_id', $giver->nid);
    $query->condition('m.nid', $exceptions, 'NOT IN');

    // Count connections
    $connections = '(SELECT COUNT(*) FROM {field_data_field_gec_to} ct WHERE ct.field_gec_to_target_id = m.nid)';
    $query->addExpression($connections, 'connections');
    $query->havingCondition('connections', $gifts, '<');

    // Remove connections.
    $connections = db_select('field_data_field_gec_to', 'ct')
      ->fields('ct', array('field_gec_to_target_id'));
    $connections->join('field_data_field_gec_from', 'cf', 'cf.entity_id = ct.entity_id');
    $connections->condition('cf.field_gec_from_target_id', $giver->nid);
    $query->condition('m.nid', $connections, 'NOT IN');

    // Order by.
    $query->orderBy('exceptions', 'DESC')
      ->orderBy('connections', 'ASC')
      ->orderRandom();

    // Group by.
    $query->groupBy('m.nid');

    $receiver = $query->execute()->fetchAssoc();

    if (isset($receiver['nid'])) {
      gift_exchange_create_connection($giver->nid, $receiver['nid'], $party_nid);
    }
    else {
      // Check if party has reached its circles limit and declare unsolvable.
      $circles_limit = variable_get('ge_circles', 5);
      if ($party->field_ge_circles[LANGUAGE_NONE][0]['value'] >
        $circles_limit) {
        watchdog('gift_exchange', 'Party :title is unsolvable.', array(':title' => $party->title));
        $party->field_ge_circles[LANGUAGE_NONE][0]['value'] = 0;
        node_save($party);

        // Change status to unsolvable.
        gift_exchange_set_party_status($party_nid, 'unsolvable_reset');

        return;
      }

      // Add circles.
      $party->field_ge_circles[LANGUAGE_NONE][0]['value'] =
        $party->field_ge_circles[LANGUAGE_NONE][0]['value'] + 1;
      node_save($party);

      // When no more connections can be made, we delete previous connections to
      // rerun. We delete (1/3 * members)(maximum 30) connections.
      $query = db_select('node', 'm')
        ->fields('m', array('nid'))
        ->condition('gem.field_gift_exchange_target_id', $party_nid)
        ->condition('m.type', 'gift_exchange_member');
      // Members of the party.
      $query->join('field_data_field_gift_exchange', 'gem',
        'gem.entity_id = m.nid');
      $result = $query->execute()->rowCount();
      $limit_delete_connections = ceil($result/3) > 30 ? 30 : ceil($result/3);

      $query = db_select('node', 'c')
        ->fields('c', array('nid'))
        ->condition('gem.field_gift_exchange_target_id', $party_nid)
        ->condition('c.type', 'gift_exchange_connection')
        ->condition('cf.field_gec_from_target_id', $giver->nid, '!=')
        ->condition('ct.field_gec_to_target_id', $giver->nid, '!=')
        ->range(0, $limit_delete_connections);

      // Party.
      $query->join('field_data_field_gift_exchange', 'gem',
        'gem.entity_id = c.nid');

      // Avoid to delete connections of current member.
      $query->leftjoin('field_data_field_gec_from', 'cf', 'c.nid = cf.entity_id');
      $query->leftjoin('field_data_field_gec_to', 'ct', 'c.nid = ct.entity_id');

      // Random order.
      $query->orderRandom();

      $result = $query->execute()->fetchAll();

      // Deleting connections.
      foreach ($result as $item) {
        watchdog('gift_exchange', 'Deleting connection :item.', array(':item' => $item->nid));
        node_delete($item->nid);
      }
    }
  }
}

/**
 * Delete connections from parties that are in reset.
 */
function gift_exchange_reset_party() {
  // Parties in reset.
  $query = db_select('node', 'ge')
    ->fields('ge', array('nid'))
    ->condition('ge.type', 'gift_exchange')
    ->range(0, 10);

  $or = db_or();
  $or->condition('s.field_ge_status_value', 'reset');
  $or->condition('s.field_ge_status_value', 'unsolvable_reset');
  $query->condition($or);

  $query->join('field_data_field_ge_status', 's',
    's.entity_id = ge.nid');

  $parties = $query->execute()->fetchAll();

  // Deleting connections.
  foreach ($parties as $party) {
    // Reset circles.
    $party_loaded = node_load($party->nid);
    $party_loaded->field_ge_circles[LANGUAGE_NONE][0]['value'] = 0;
    node_save($party_loaded);

    $query = db_select('node', 'c')
      ->fields('c', array('nid'))
      ->condition('gem.field_gift_exchange_target_id', $party->nid)
      ->condition('c.type', 'gift_exchange_connection')
      ->range(0, 50);

    // Party.
    $query->join('field_data_field_gift_exchange', 'gem',
      'gem.entity_id = c.nid');

    $connections = $query->execute()->fetchAll();
    if (count($connections)) {
      $connections_array = array();
      foreach ($connections as $connection) {
        $connections_array[] = $connection->nid;
      }
      node_delete_multiple($connections_array);
    }
    else {
      $party_status = gift_exchange_get_party_status($party->nid);
      if ($party_status == 'reset') {
        // Set party back to setup.
        gift_exchange_set_party_status($party->nid, 'setup');
      }
      elseif ($party_status == 'unsolvable_reset') {
        // Set party back to unsolvable.
        gift_exchange_set_party_status($party->nid, 'unsolvable');
      }
    }
  }
}

/**
 * Auto archive old parties.
 */
function gift_exchange_archive_parties() {

  $timestamp = time();
  
  $query = db_select('node', 'ge')
    ->fields('ge', array('nid'))
    ->condition('ge.type', 'gift_exchange')
    ->condition('s.field_ge_status_value', 'archived', '!=');

  // Archive parties based on settings or end date.
  $or = db_or();
  $or->condition('ge.created', $timestamp - ((int)variable_get('ge_archive', 365) * 86400), '<');
  $or->condition('e.field_ge_exchange_date_value', $timestamp, '<');

  $query->condition($or);

  // Status.
  $query->join('field_data_field_ge_status', 's',
    's.entity_id = ge.nid');

  // Exchange date.
  $query->join('field_data_field_ge_exchange_date', 'e',
    'e.entity_id = ge.nid');

  $result = $query->execute()->fetchAll();

  $num_update = count($result);

  if ($num_update) {
    foreach ($result as $item) {
      gift_exchange_set_party_status($item->nid, 'archived');
    }

    watchdog('gift_exchange', ':num parties archived.', array(':num' => $num_update));
  }
}

/**
 * Auto refresh page.
 *
 * @param $interval
 *   Interval of auto refresh.
 */
function gift_exchange_auto_refresh($interval) {
  $element = array(
    '#tag' => 'meta',
    '#attributes' => array(
      'http-equiv' => 'refresh',
      'content' => $interval,
    ),
  );
  drupal_add_html_head($element, 'refresh');
}
