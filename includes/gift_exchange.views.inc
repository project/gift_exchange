<?php

/**
 * @file
 * Views extend file for Gift Exchange module.
 */

/**
 * Implements hook_views_data().
 */
function gift_exchange_views_data() {

  // Field handler for item Gift Exchange Party status.
  $data['node']['gift_exchange_party_status'] = array(
    'field' => array(
      'title' => t('Party status'),
      'help' => t('Gift Exchange Party status.'),
      'handler' => 'views_handler_field_gift_exchange_views_functions_status',
    ),
  );

  // Party member count.
  $data['node']['gift_exchange_party_member_count'] = array(
    'field' => array(
      'title' => t('Party member count'),
      'help' => t('Gift Exchange Party member count.'),
      'handler' => 'views_handler_field_gift_exchange_views_functions_member_count',
    ),
  );

  // Filter for parties where current user is a giver.
  $data['node']['current_user_mail'] = array(
    'title' => t('Current user is giver'),
    'help' => t('Filter Gift Exchange parties based on current user being a giver.'),
    'filter' => array(
      'handler' => 'gift_exchange_handler_filter_current_user_giver',
     ),
  );

  return $data;
}

/**
 * Field handler for item Gift Exchange Party status.
 *
 * @ingroup views_field_handlers
 */
class views_handler_field_gift_exchange_views_functions_status extends views_handler_field {

  function construct() {
    parent::construct();
  }

  function option_definition() {
    $options = parent::option_definition();
    return $options;
  }

  function query() {
    $this->ensure_my_table();
    $this->add_additional_fields();
  }

  function render($values) {
     // Load functions.
    $file_path = module_load_include('inc', 'gift_exchange', 'includes/gift_exchange.functions');
    return gift_exchange_get_party_status_display($values->nid);
  }
}

/**
 * Field handler for item Gift Exchange Party status.
 *
 * @ingroup views_field_handlers
 */
class views_handler_field_gift_exchange_views_functions_member_count extends views_handler_field {

  function construct() {
    parent::construct();
  }

  function option_definition() {
    $options = parent::option_definition();
    return $options;
  }

  function query() {
    $this->ensure_my_table();
    $this->add_additional_fields();
  }

  function render($values) {
     // Load functions.
    $file_path = module_load_include('inc', 'gift_exchange', 'includes/gift_exchange.functions');
    return gift_exchange_get_party_members_count($values->nid);
  }
}

/**
 * Filter for parties where current user is a giver.
 */
class gift_exchange_handler_filter_current_user_giver extends views_handler_filter {

  function admin_summary() { }
  function operator_form(&$form, &$form_state) { }
  function can_expose() {
    return FALSE;
  }

  function query() {
    $table = $this->ensure_my_table();
    $member_email = $this->query->ensure_table('field_data_field_gem_email', $this->relationship);

    global $user;
    $this->query->add_where_expression($this->options['group'], $member_email . ".field_gem_email_value = '" . $user->mail . "'");
  }
}
