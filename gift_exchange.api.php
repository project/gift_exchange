<?php

/**
 * @file
 * Hooks provided by Gift Exchange module.
 */

/**
 * Hook that runs before party page is built.
 *
 * @param $form
 *   Party form.
 * @param $node
 *   Party node.
 *
 * @return array
 *   Party form altered.
 */
function hook_ge_party_prebuild($form, $node) {

  $form['main_container']['info']['header'] = array(
    '#type' => 'html_tag',
    '#value' => t('This is a party'),
    '#tag' => 'h3',
  );

  return $form;
}

/**
 * Party view alter.
 *
 * @param $form
 *   Party form.
 * @param $node
 *   Party node.
 *
 * @return array
 *   Party form altered.
 */
function hook_ge_party_view_alter($form, $node) {

  $form['main_container']['info']['header'] = array(
    '#type' => 'html_tag',
    '#value' => t('This is a party'),
    '#tag' => 'h3',
  );

  return $form;
}

/**
 * Check access for a Gift Exchange node.
 *
 * @param $node
 *   Gift Exchange node.
 *
 * @return boolean
 *   TRUE or FALSE.
 */
function hook_geg_access($node) {

  return ($node->uid == 1);
}

/**
 * Alter new member. It runs before a new member is saved.
 *
 * @param $member
 *   Member.
 * @return $member
 *   Member altered.
 */
function hook_ge_member_new($member) {

  $member->title = '[Altered]' . $member->title;

  return $member;
}

/**
 * Alter new connection. It runs before a new connection is saved.
 *
 * @param $connection
 *   Connection.
 * @return $connection
 *   Connection altered.
 */
function hook_ge_connection_new($connection) {

  $connection->title = '[Altered]' . $connection->title;

  return $connection;
}

/**
 * Alter new exception. It runs before a new exception is saved.
 *
 * @param $exception
 *   Exception.
 * @return $exception
 *   Exception altered.
 */
function hook_ge_exception_new($exception) {

  $exception->title = '[Altered]' . $exception->title;

  return $exception;
}

/**
 * On status change of Party.
 *
 * @param $party
 *   Gift Exchange Party.
 *
 * @return $party
 *   Party altered.
 */
function hook_ge_party_status_changed($party, $previous_status, $new_status) {

  watchdog('my_module', t('Party :title changed state to :state',
    array(':title' => $party->title, ':state' => $new_status)));

  return $party;
}
